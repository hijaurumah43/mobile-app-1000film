import Api from './Api'

export default {
  load() {
    return Api().get('/initload');
  },
}