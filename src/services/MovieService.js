import Api from './Api'

export default {
    movies(data) {
        return Api().post('/list-video', data);
    },
    payment(data) {
        return Api().post('/payment-qris', data);
    },
    getVa(data) {
        return Api().post('/get-va', data);
    },
    getCC(data) {
        return Api().post('/cc', data);
    },
    getQris(data) {
        return Api().post('/get-qris', data);
    },
    paymentCheck(data) {
        return Api().post('/cek-payment', data);
    },
    requestTicket(data) {
        return Api().post('/request-ticket', data);
    },
    showMovie(data, idMovie) {
        return Api().post('/show-video?id='+ idMovie, data);
    },
    playMovie(idUser, idToken, idMovie, idSignature, xKey) {
        return Api().get('/play-video?idu='+ idUser + '&idt=' + idToken + '&idm=' + idMovie + '&ids=' + idSignature + '&xkey='+ xKey);
    },
    updatePassword(data) {
        return Api().post('/update-password', data);
    },
    historyPayment(data) {
        return Api().post('/history-payment', data);
    },
    updateProfile(data) {
        return Api().post('/update-profile', data);
    },
    referal(data) {
        return Api().post('/referral', data);
    },
}