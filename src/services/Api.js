import axios from 'axios'

export default () => {
	// return axios.create({
	// 	baseURL: `http://127.0.0.1:8000/api`,
	// 	withCredentials: false,
	// 	headers: {
	// 		'Authorization': 'Bearer ' + localStorage.getItem('access-token'),
	// 		'Accept': 'application/json',
	// 		'Content-Type': 'application/json'
	// 	}
	// })

	let headers = {
			'cache-control': 'no-cache'
	};
	let accessToken = localStorage.getItem('access-token');

	if (accessToken && accessToken !== '') {
			headers.Authorization = accessToken;

	}
	const instance = axios.create({
		baseURL: `https://api.yukflix.com`,
		// baseURL: `http://localhost/kinarya/api3/`,
		withCredentials: false,
		headers: {
			// 'signature': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFzZGFzIiwicGFzcyI6ImFzZGFzZCJ9.Fey95qoNxK3W6o9xetxNjF9OukD3ge0QKE3ikOCEhjw',
			// 'Authorization': 'Bearer ' + localStorage.getItem('access-token'),
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	});

	instance.interceptors.response.use((response) => {
			if(response.status === 401) {
					//add your code
					console.log('wk')
					alert("You are not authorized");
			}
			return response;
	}, (error) => {
			if (error.response && error.response.data) {
					//add your code
					// console.log(error.response.status)
					localStorage.removeItem("userInfo");
					localStorage.removeItem("access-token");
					localStorage.removeItem("profile");
					// this.$router.push('app/sessions/signIn'); 
					// this.$router.push('app/sessions/signIn'); 
					
					// return Promise.reject(error.response.data);
			}
			// console.log('hello')
			return Promise.reject(error.message);
	});

	return instance;
}
