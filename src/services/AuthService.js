import Api from './Api'

export default {
    login(data) {
        return Api().post('/login', data);
    },
    register(data) {
        return Api().post('/sign-up', data);
    },
    me() {
        return Api().get('/profile');
    },
    activation(id, regcode) {
        return Api().post('/confirm-account?id=' + id + '&regcode=' + regcode);
    },
    forgotPassword(data) {
        return Api().post('/ask-reset-password', data);
    },
    resetPassword(data) {
        return Api().post('/reset-password', data);
    },
}